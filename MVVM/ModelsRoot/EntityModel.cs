﻿using MVVM.ViewModels;

namespace MVVM.Models
{
    public abstract class EntityModel : IEntity
    {
        public string EntityId { get; set; }
        public EntityModel(string entityId)
        {
            EntityId = entityId;
        }
    }
}