﻿using System.Collections.Generic;
using Assisticant.Collections;

namespace MVVM.Models
{
    public class ListModel<T>
    {
        protected ObservableList<T> _itemList = new ObservableList<T>();

        public IEnumerable<T> ItemList => _itemList;
        public readonly Selection<T> ItemSelection;

        public ListModel()
        {
            ItemSelection = new Selection<T>();
        }
    }
}