﻿using Assisticant.Fields;

namespace MVVM.Models
{
    public class Selection<T>
    {
        readonly Observable<T> _selectedItem = new Observable<T>();
        public T SelectedItem
        {
            get => _selectedItem.Value;
            set => _selectedItem.Value = value;
        }
    }

    public class BookSelection
    {
        readonly Observable<Book> _selectedItem = new Observable<Book>();
        public Book SelectedItem
        {
            get => _selectedItem.Value;
            set => _selectedItem.Value = value;
        }
    }
}