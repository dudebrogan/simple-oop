﻿namespace MVVM.Models
{
    public class BooksListModel : ListModel<Book>
    {
        public BooksListModel() : base()
        {
            _itemList.Add(new Book("kitty", "A Tail of Two Kitties", "Darla's Chickens"));
            _itemList.Add(new Book("peace", "Par and Weeds", "Theo Loves Toy"));
            _itemList.Add(new Book("cars", "A Truck's Cousin", "No one wrote this"));
            _itemList.Add(new Book("name", "Top Three Bad Book Names", "Nathan Bogan"));
        }

        public void Books()
        {
            var index = _itemList.IndexOf(ItemSelection.SelectedItem);
            var otherIndex = index < _itemList.Count - 1 ? index + 1 : 0;
            var otherItem = _itemList[otherIndex];
            var newItemName = otherItem.Name;
            var newItemAuthor = otherItem.Author;
            otherItem.Name = ItemSelection.SelectedItem.Name;
            otherItem.Author = ItemSelection.SelectedItem.Author;
            ItemSelection.SelectedItem.Name = newItemName;
            ItemSelection.SelectedItem.Author = newItemAuthor;
        }
    }
}