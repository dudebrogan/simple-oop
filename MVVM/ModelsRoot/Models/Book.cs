﻿using Assisticant.Fields;
using MVVM.ViewModels;

namespace MVVM.Models
{
    public class Book : EntityModel
    {
        readonly Observable<string> _name = new Observable<string>();
        readonly Observable<string> _author = new Observable<string>();

        public string Name
        {
            get => _name.Value;
            set => _name.Value = value;
        }
        
        public string Author
        {
            get => _author.Value;
            set => _author.Value = value;
        }

        public Book(string entityId, string name, string author) : base(entityId)
        {
            Name = name;
            Author = author;
        }
    }
}