﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using MVVM.Models;

namespace MVVM.ViewModels
{
    public class BooksListViewModel : ListViewModel<BookHeaderViewModel, Book, BooksListModel>
    {
        public IEnumerable<BookHeaderViewModel> Books =>
            Model.ItemList.Select(book => new BookHeaderViewModel(book));
        
        public BookHeaderViewModel SelectedBookHeader
        {
            get => GetListSelection();
            set => SelectedItem = value?.Entity;
        }

        public BookDetailViewModel SelectedBookDetail => SelectedItem == null ? null : new BookDetailViewModel(SelectedItem);

        public ICommand SwitchBooks => new Command(() => Model.Books());
    }
}