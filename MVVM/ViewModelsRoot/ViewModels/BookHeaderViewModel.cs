﻿using System;
using MVVM.Models;

namespace MVVM.ViewModels
{
    public class BookHeaderViewModel : EntityViewModel<Book>
    {
        public string Name => Entity.Name;

        public BookHeaderViewModel(){}
        public BookHeaderViewModel(Book book)
        {
            Entity = book;
        }
    }
}