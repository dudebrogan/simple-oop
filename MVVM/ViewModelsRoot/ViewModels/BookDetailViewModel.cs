﻿using System;
using MVVM.Models;

namespace MVVM.ViewModels
{
    public class BookDetailViewModel : EntityViewModel<Book>
    { 
        public string Author => Entity.Author;

        public BookDetailViewModel(Book book)
        {
            Entity = book;
        }
    }
}