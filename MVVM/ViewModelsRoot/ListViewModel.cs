﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using MVVM.Models;

namespace MVVM.ViewModels
{
    public abstract class ListViewModel<TEntityViewModel, TEntity, TModel> : ViewModel 
        where TModel : ListModel<TEntity>
        where TEntity : EntityModel
        where TEntityViewModel : EntityViewModel<TEntity>, new()
    {
        public TModel Model;
        
        public TEntity SelectedItem
        {
            get => Model.ItemSelection.SelectedItem;
            set => Model.ItemSelection.SelectedItem = value;
        }

        protected TEntityViewModel GetListSelection()
        {
            if (SelectedItem != null)
                return GetEntityViewModel();
            if (Model.ItemList.Any())
                SelectedItem = Model.ItemList.FirstOrDefault();
            else return null;
            return GetEntityViewModel();
        }

        TEntityViewModel GetEntityViewModel() => 
            new TEntityViewModel
        {
            Entity = SelectedItem
        };
    }
}