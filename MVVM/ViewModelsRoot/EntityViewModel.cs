﻿using System;

namespace MVVM.ViewModels
{
    public class EntityViewModel<T> : ViewModel where T : IEntity
    {
        public T Entity;

        protected override object IdentityKey() => Entity?.EntityId;
    }
}