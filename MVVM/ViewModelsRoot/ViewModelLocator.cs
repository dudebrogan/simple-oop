﻿using Assisticant;
using MVVM.Models;

namespace MVVM.ViewModels
{
    public class ViewModelLocator : ViewModelLocatorBase
    {
        BooksListModel BooksListModel { get; } = new BooksListModel();
        
        public object BooksPage => ViewModel(() => new BooksListViewModel
        {
            Model = BooksListModel
        });
    }
}