﻿using System;
using System.Data;
using MVVM.Models;

namespace MVVM.ViewModels
{
    public abstract class ViewModel
    {
        protected virtual object IdentityKey() => null;
        
        // assisticant uses these methods to determine if a view model needs remade
        // we override to make sure we aren't constantly recreating view models wastefully
        // rather than override in every view model class, view models that wrap
        // entities can just reuse this
        public override bool Equals(object obj)
        {
            if (IdentityKey() == null) return base.Equals(obj);
            return obj?.GetType() == GetType() && Object.Equals(((ViewModel) obj).IdentityKey(), IdentityKey());
        }
        
        public override int GetHashCode()
        {
            if (IdentityKey() == null) return base.GetHashCode();
            return IdentityKey().GetHashCode();
        }
    }
}