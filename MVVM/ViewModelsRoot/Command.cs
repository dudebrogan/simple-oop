﻿using System;
using System.Windows.Input;

namespace MVVM.ViewModels
{
    class Command : ICommand
    {
        Action Go;

        public Command(Action go)
        {
            Go = go;
        }

        public bool CanExecute(object parameter) => true;

        public void Execute(object parameter)
        {
            Go();
        }

        public event EventHandler CanExecuteChanged;
    }
}