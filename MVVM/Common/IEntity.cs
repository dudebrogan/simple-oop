﻿namespace MVVM.ViewModels
{
    public interface IEntity
    {
        string EntityId { get; set; }
    }
}